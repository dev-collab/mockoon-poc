<div align="center">
    <img src="logo.png" alt="Linux Logo" width="256" />
    <h1>Using Mockoon - Proof of Concept</h1>
</div>

Mockoon is the easiest and quickest way to run mock APIs locally. No remote deployment, no account required, free and open-source.

Our project's main goal is to offer a proof of concept for using Mockoon through the use of documented convenience scripts to run its getting-started demo, but also to provide a demo of our own for an in-depth look at the tool.

<div align="center">
    <img src="screen.png" width="824" alt="Screenshot"/>
</div>

# Prerequisites

## System Requirements

- Linux Distribution with GUI compatibility (Desktop, WSL2)

- Bash / Zsh
    - GNU bash 5.*
    - Zsh 5.*

- [Snapd](https://snapcraft.io/docs/installing-snapd)
- [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

This project is regularly being tested on a variety of Linux distributions. Recently on:

- **Debian GNU/Linux 11 (bullseye) on Windows 10 x86_64**

**NB**: Using different Linux distributions and/or different versions of the above packages might affect performance.



## Recommended

For a fresh start, we recommend using the [devops-collab/linuxb-cli](https://gitlab.com/devops-collab/linuxb-cli) repository. This will allow you to have a **new clean instance** of your virtual machine up and running with the correct configuration.

# Getting Started

Open a terminal and clone the project to your `$HOME` folder:

```bash
cd $HOME && git clone https://gitlab.com/dev-collab/mockoon-poc.git
```

# Running _[Mockoon Desktop](https://github.com/mockoon/mockoon)_

## Linux Terminal

Navigate to the root of the repository:

```bash
cd ~/mockoon-poc/
```

Run `install-mockoon-desktop` script:

```bash
./install-mockoon-desktop.sh
```

Open Mockoon Desktop:

```bash
mockoon
```

**NB**: Once installed you may run the `mockoon` command directly.

# Running _[Mockoon CLI](https://github.com/mockoon/mockoon/tree/main/packages/cli)_

Run `install-mockoon-cli` script:

```bash
./install-mockoon-cli.sh
```

Open Mockoon Desktop:

```bash
mockoon-cli
```

**NB**: Once installed you may run the `mockoon-cli` command directly.

# Running _[python-flask demo](https://gitlab.com/thibaultcharrin/serverless-python)_



## All done

Thank you for using Mockoon

### Sources

[Getting-Started](https://mockoon.com/tutorials/getting-started/),
[Mockoon](https://github.com/mockoon/mockoon),
[Mockoon-CLI](https://github.com/mockoon/mockoon/tree/main/packages/cli)

# Credits

- [Guillaume Monnet](https://github.com/255kb) (_Full-Stack Web Developer_)
    
    - Founder of Mockoon, main contributor

- [Mockoon Community](https://github.com/mockoon) (GitHub namespace)

    - Contributors, sponsors, support

- [Thibault Charrin](https://gitlab.com/tcharrin) (_DevOps Consultant_)

  - POC author, script implementation

- [devler](https://macosicons.com/#/u/devler)

  - Logo
